package com.redd.service;

import org.springframework.stereotype.Service;

import java.security.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class KeysGenerator
{
    private final ServiceFacade serviceFacade;

    public KeysGenerator(ServiceFacade serviceFacade)
    {
        this.serviceFacade = serviceFacade;
    }

    public void run(String algorithmName, int keySize, boolean fileSwitch) throws NoSuchAlgorithmException,
                                                                                  InstantiationException,
                                                                                  IllegalAccessException
    {
        KeyPair keyPair = generateKeyPair(algorithmName, keySize);
        List<byte[]> encodedKeys = encodeKeys(keyPair);

        serviceFacade
                .keySplitter()
                .run(encodedKeys, fileSwitch);
    }

    private KeyPair generateKeyPair(String algorithmName, int keySize) throws NoSuchAlgorithmException
    {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(algorithmName);
        keyPairGen.initialize(keySize);
        return keyPairGen.generateKeyPair();
    }

    public List<byte[]> encodeKeys(KeyPair keyPair)
    {
        List<byte[]> encodedKeys = new ArrayList<>();

        final Base64.Encoder encoder = Base64.getEncoder();
        final PrivateKey privateKey = keyPair.getPrivate();
        final PublicKey publicKey = keyPair.getPublic();

        byte[] privateKeyEncoded = encoder.encode(privateKey.getEncoded());
        byte[] publicKeyEncoded = encoder.encode(publicKey.getEncoded());
        encodedKeys.add(privateKeyEncoded);
        encodedKeys.add(publicKeyEncoded);

        return encodedKeys;
    }
}
