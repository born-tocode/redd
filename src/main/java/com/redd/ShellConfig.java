package com.redd;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.commands.Clear;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;

@Configuration
@ShellComponent
public class ShellConfig implements PromptProvider, Clear.Command
{

    @ShellMethod(value = "Clear the shell screen.", group = "Built-In Commands")
    public void clear()
    {
        final int maxLoops = 500;

        for (int i = 0; i < maxLoops; i++)
        {
            printWriter().println();
        }
    }

    @Override
    public AttributedString getPrompt()
    {
        return new AttributedString("shell:#", AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW));
    }

    @Bean
    public PrintWriter printWriter()
    {
        return new PrintWriter(new OutputStreamWriter(System.out), true);
    }

}
